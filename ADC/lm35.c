#include "lm35.h"

void adc_init(void) {
    ADMUX |= (1 << MUX0) | (1 << MUX2);  // set ADC multiplexer to output ADC5
    ADMUX |= (1 << REFS0) | (1 << REFS1); // VREF set to internal 2.56V
    ADCSRA |= (1 << ADPS0) | (1<<ADPS1) | (1<<ADPS2); // set frequency of measure with precaler 128
    ADCSRA |= (1 <<ADEN);  // enable ADC
}

uint16_t measure_temp(void) {
    ADCSRA |= (1 << ADSC); // start conversion
    while(bit_is_set(ADCSRA, ADSC)); // wait for end conversion
    return ADCW/4;
    /*
    LM35 - 10mv per C degree
    For example 25 C degrees = 0.25V
    ADC = (voltage * 10bit resolution ADC) / ref voltage;
    ADC = (0.25 * 1024) / 2.56V
    ADC = 100, so ADC/4 = temperature (C degrees)
    */
}