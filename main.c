#include <util/delay.h>
#include "LCD/lcd44780.h"
#include "ADC/lm35.h"

int main( void ) {

    lcd_init();
    adc_init();

    lcd_cls();
    lcd_locate(0,0);
    lcd_str("Temperatura:");

    while ( 1 ) {

        lcd_locate(1,0);
        lcd_int(measure_temp());
        lcd_str(" st C");
        _delay_ms(1000);
    }
}